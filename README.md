# Devops Bitbucket Addon

## Configuration

There are 2 different ways of configuring the add-on. You can use the setup from within the add-on or use a configuration file and generate the inputs.conf before installing the add-on as described below.

* Configure the *config.properties* with the following properties :
  
  - host : URL of the Bitbucket server (ex : bitbucket.accenture.com)
  - protocol : protocol to use, http or https
  - project : Name of the project of the repository (ex : A1220)
  - repositories : Name of the repositories separated by a comma to generate multiple data inputs (ex : aiam-hpdp-integration)
  - auth_type : Type of authentication used to access the Bitbucket server (ex :basic)
  - auth_user : Username used to log in
  - auth_password : Password used to log in, this must be a base64 encoded string (use ``` echo -n 'password' | base64``` to encode)

* Additional optional parameters may also be over-written:
  - sourcetype : name of the sourcetype (default is bitbucket_api)
  - index : Name of the index we want to send the data (default is aaam_devops_bitbucket_idx)
  - interval : Polling time between different requests (default is 300s.)
  - url_args = Url arguments to pass to the bitbucket api (default contains pagelen=100)

* You may add as many sections as you want to include different information or ping different servers, the category name of the config file **MUST** be different

```
[Bitbucket-1]
host = api.bitbucket.org
protocol=http
project = samsungtoko
repositories = toko-auto,project2
auth_type = basic
auth_user = 
auth_password = 
interval = 60

[Bitbucket-2]
host = bitbucket.accenture.com
protocol=http
project = A1220
repositories = toko-auto
auth_type = basic
auth_user = 
auth_password = 
interval = 60
index = aaam_devops_bitbucket_acn_idx
```

* Generate the final configuration :
  
    python generate_bitbucket_config.py <config>
    
* Check the *default/inputs.conf* generated has all of your configuration parameters


## Package

* Create a tarball of this repository :

    tar czvf aaam-devops-bitbucket-addon.tar.gz aaam-devops-bitbucket-addon
    
## Installation 

* Upload this archive into splunk with the Manage Apps UI screen
* Don't forget to create the index **aaam_devops_bitbucket_idx** if it is not done

## Search 

* data is present in the  **aaam_devops_bitbucket_idx** index

