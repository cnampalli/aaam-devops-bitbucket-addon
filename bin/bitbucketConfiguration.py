import ConfigParser
import os.path
from abc import ABCMeta, abstractmethod
import sys
import base64

#############
# Class used to create inputs.conf file
#############

class AbstractAppConfiguration:
  __metaclass__ = ABCMeta

  def __init__(self, app_dir=None):
    if not app_dir:
      app_name = __file__.split(os.sep)[-3]
      app_dir = os.path.join(os.environ["SPLUNK_HOME"], 'etc', 'apps', app_name)
    self.configInputsFile = os.path.join(app_dir, 'default', 'inputs.conf')
    self.configFile = os.path.join(app_dir, 'config.properties')

  def addParameters(self, inputConfig, inputSection, outputConfig, outputSection, parameters):
    """ Adds a parameters to a config file using an input config, add a hashmap containg default values if the value is missing in the input
        inputConfig -- ConfigParser to read the value from
        inputSection -- Name of the input section to read the value from
        outputConfig -- Output configuration object
        outputSection -- Section of the outputConfig in which to write the parameter
        parameters -- Hashmap containing default values
    """
    for parameter in parameters:
      if inputConfig.has_option(inputSection, parameter):
        parameterValue = inputConfig.get(inputSection, parameter)
        outputConfig.set(outputSection, parameter, parameterValue)
      elif parameters[parameter]:
        parameterValue = parameters[parameter]
        outputConfig.set(outputSection, parameter, parameterValue)
      else:
        print "WARN: parameter {0} is missing in section {1}".format(parameter, outputSection)  

  def addAuthenticationConfiguration(self, inputConfig, inputSection, outputConfig, outputSection):
    """ Adds specific authentication parameters
    """
    auth_type = inputConfig.get(inputSection, 'auth_type')
    outputConfig.set(outputSection, 'auth_type', auth_type)
    parameters ={
      'none'   : [],
      'basic'  : ['auth_user', 'auth_password'],
      'digest' : ['auth_user', 'auth_password'],
      'oauth1' : ['oauth1_client_key', 'oauth1_client_secret', 'oauth1_access_token', 'oauth1_access_token_secret'],
      'oauth2' : ['oauth2_token_type', 'oauth2_access_token', 'oauth2_refresh_token', 'oauth2_refresh_url', 'oauth2_refresh_props', 'oauth2_client_id', 'oauth2_client_secret' ],
      'custom' : []
    }[auth_type]
    self.addParameters (inputConfig, inputSection, outputConfig, outputSection, parameters)

  @abstractmethod
  def handleSection():
    pass

  def createInputsConfiguration(self, configPath=None, fileWriteMode='w'):
    """Creates or appends to the existing configuration file
    """
    if not configPath:
      configPath = self.configFile
    if os.path.isfile(str(configPath)) :
      inputConfig = ConfigParser.RawConfigParser()
      inputConfig.read(configPath)
      with open(self.configInputsFile, fileWriteMode) as configInputsFile:
        outputConfig = ConfigParser.RawConfigParser()
        for inputSection in inputConfig.sections():
          self.handleSection(inputConfig, inputSection, outputConfig)
        outputConfig.write(configInputsFile)
    else:
      print ('%s is not  a correct input file' % configPath)

  def appendToInputConfiguration(self, inputConfig):
    """Creates or appends to the existing configuration file
    """
    with open(self.configInputsFile, 'a') as configInputsFile:
      outputConfig = ConfigParser.RawConfigParser()
      for inputSection in inputConfig.sections():
        self.handleSection(inputConfig, inputSection, outputConfig)

      # Encode password
      for section in outputConfig.sections():
        encoded_pwd = base64.b64encode(config.get(section, 'auth_password'))
        config.set(section, 'auth_password', encoded_pwd)
      outputConfig.write(configInputsFile)

  def readConfig(self):
    if os.path.isfile(str(self.configFile)) :
      config = ConfigParser.RawConfigParser()
      config.read(self.configFile)

      # Decode passwords
      for section in config.sections():
        encoded_pwd = base64.b64decode(config.get(section, 'auth_password'))
        config.set(section, 'auth_password', encoded_pwd)
      return config
    else:
      print ('%s is not  a correct input file' % self.configFile)

  def writeConfig(self, config):
    # Encode password
    for section in config.sections():
      encoded_pwd = base64.b64encode(config.get(section, 'auth_password'))
      config.set(section, 'auth_password', encoded_pwd)

    with open(self.configFile, 'w') as configFile:
      config.write(configFile)
####
# Specific bitbucket
####
class AppConfiguration(AbstractAppConfiguration):
  def __init__(self, app_dir=None):
    super(AppConfiguration, self).__init__(app_dir)
    self.section = 'bitbucket://{project}-{repo}'
    self.endpoint = '{protocol}://{host}/2.0/repositories/{project}/{repo}/commits/{branch}?pagelen=100'
    self.default_configuration = {       
        'auth_type' : 'basic',
        'auth_user': '',
        'auth_password': '',
        'http_method': 'GET',
        'index_error_response_codes': '0',
        'response_type': 'json',
        'sequential_mode' : '0',
        'sourcetype': 'bitbucket_api',
        'streaming_request' : '0',
        'index' : 'aaam_devops_bitbucket_idx',
        'disabled' : '0',
        'response_handler' : 'Bitbucket',
        'interval' : '300',
        'url_args' : ''
    }
  pass


  def handleSection(self, inputConfig, inputSection, outputConfig):
    host = inputConfig.get(inputSection, 'host')
    protocol = inputConfig.get(inputSection, 'protocol')
    project = inputConfig.get(inputSection, 'project')
    repositories = inputConfig.get(inputSection, 'repositories')
    branch = inputConfig.get(inputSection, 'branch')
      
    for repo in repositories.split(','):
      repo = repo.strip()
      outputSection = self.section.format(project=project, repo=repo)
      outputConfig.add_section(outputSection)      
      outputConfig.set(outputSection, 'endpoint', self.endpoint.format(protocol=protocol, host=host, project=project, repo=repo, branch=branch))
      self.addAuthenticationConfiguration(inputConfig, inputSection, outputConfig, outputSection)
      self.addParameters(inputConfig, inputSection, outputConfig, outputSection, self.default_configuration)


