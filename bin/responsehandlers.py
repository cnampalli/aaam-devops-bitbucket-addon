#add your custom response handler class to this module
import json, requests
import datetime
import time
import re
import logging
import requests
class Bitbucket:

    def __init__(self,**args):
        pass

    def __call__(self, response_object,raw_response_output,response_type,req_args,endpoint):
        if response_type == "json":
            output = json.loads(raw_response_output)
            logging.info("the url needed is %s " %response_object.url)
            logging.info("requirement arguments are %s " %req_args)
            #logging.info("requirement arguments for new nurl %s" %(type(req_args["params"]["new_url"])))
            
            new_url = None
          
            #logging.info("Response output is here>>>>>>>>>>>>>> %s" %new_url)

            newLastId = None
            if len(output["values"]) > 0:
                newLastId = output["values"][0]["hash"]
                logging.info("printing last id>> %s" %newLastId)

            lastId = None
            if not "params" in req_args:
                req_args["params"] = {}
            # elif (("lastId" in req_args["params"]) and (req_args["params"]["new_url"] != "None") ):
            #     logging.info("coming to fetch the new url")
            #     newLastId = req_args["params"]["lastId"]
            #     new_url = req_args["params"]["new_url"]
            #     req_args["params"]["lastId"] = None
            #     req_args["params"]["new_url"] = None
            #     #next_response = requests.get(new_url)
            #     session = requests.Session()
            #     next_response = session.get(new_url, **req_args)
            #     next_response.raise_for_status()
            #     logging.info("Response of commits Endpoint is %s" %(next_response))
            #     #next_response = json.loads(next_response.text)
            #     output =json.loads(next_response.text)
            #     logging.info("jksdfasjkdh%s " %(output))
                #next_response = requests.get(req_args["params"]["new_url"], **req_args)
                #output =json.loads(next_response.text)

            #pagination loop
            isLastPage = False
            while not isLastPage:
                for record in output["values"]:
                    #logging.info("the first record is here >>>>> %s" %(json.dumps(record)))
                    if lastId is not None and record["hash"] == lastId:
                        isLastPage = True
                        break;
                    else:
                        print_xml_stream_with_time(json.dumps(record), record["date"])

                if not isLastPage:
                    if "next" in output:
                        logging.info("printing the url of the next object %s " %(output["next"]))
                        next_response = requests.get(output["next"], **req_args)
                        pagenumber = output["next"].split("=")
                        logging.info(" page number is %s" %(pagenumber[1]))
                        if (str(next_response)[11:14] == "429"):
                            new_url = output["next"]
                            session = requests.Session()
                            next_response = session.get(new_url, **req_args)
                            next_response.raise_for_status()
                            logging.info("Response of commits Endpoint is %s" %(next_response))
                            #next_response = json.loads(next_response.text)
                            output =json.loads(next_response.text)
                            logging.info("jksdfasjkdh%s " %(output))
                            #req_args["params"]["new_url"] = output["next"]
                            #isLastPage = True
                            #break
                        else:
                            output = json.loads(next_response.text)
                            isLastPage = False
                    else:
                        #req_args["params"]["new_url"] = None
                        isLastPage = True
                            

            req_args["params"]["lastId"] = newLastId
                        

        else:
            print_xml_stream(raw_response_output)

#HELPER FUNCTIONS
    
# prints XML stream
def print_xml_stream(s):
    print "<stream><event unbroken=\"1\"><data>%s</data><done/></event></stream>" % encodeXMLText(s)

def print_xml_stream_with_time(s, date):
    date  = re.sub('\+.*$', '', date)
    timestamp = time.mktime(datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%S").timetuple())
    print "<stream><event unbroken=\"1\"><time>%s</time><data>%s</data><done/></event></stream>" % (encodeXMLText(str(timestamp)), encodeXMLText(s))



def encodeXMLText(text):
    text = text.replace("&", "&amp;")
    text = text.replace("\"", "&quot;")
    text = text.replace("'", "&apos;")
    text = text.replace("<", "&lt;")
    text = text.replace(">", "&gt;")
    text = text.replace("\n", "")
    return text
